// create our client controller and get access to firebase
app.controller('EmploymentandincomeController', function ($firebaseObject, $firebaseArray, $scope, $http, $rootScope, $location, sessionService) {

    $scope.session = sessionService.get('user');
    $scope.activeTab = 0;
    $scope.showbenefitsone1=false;
    $scope.setActiveTabValue = function (index) {
        $scope.activeTab = index;
    };
    var ref = new Firebase("https://cdeals-8f387.firebaseio.com/5467899");

    var benefitsref = new Firebase("https://cdeals-8f387.firebaseio.com/5467899/applicants/0/emploment/0/benifits");


//ng repeat data for emp tab start   
    var emp_tab = new Firebase("https://cdeals-8f387.firebaseio.com/5467899/emploment");

    $scope.emploment = $firebaseObject(emp_tab);
//end  

    var config = {
        apiKey: "AIzaSyDPqELrk7wAGT7XZrTnHKtuvur40yIpSb4",
        authDomain: "cdeals-8f387.firebaseapp.com",
        databaseURL: "https://cdeals-8f387.firebaseio.com"
    };
    $scope.customers = $firebaseObject(ref);
    var fb = $firebaseObject(ref);
    // sync as object 

    var syncObject = $firebaseObject(ref);
    $scope.benefitsdata = $firebaseArray(benefitsref);

    // three way data binding
    syncObject.$bindTo($scope, 'customers');

    //var messagesRef = new Firebase('https://cdeals-8f387.firebaseio.com/customers/applicants/emploment').child('benifits');
    //var Data = $firebase(messagesRef);
    //$scope.empdata = $firebaseObject(messagesRef);

    console.log('employment data', $scope.benefitsdata);

    //$scope.applicant_tab=true;


//hiding the tickmark    
    $scope.hidealert = function () {
        $(".alert-success").fadeTo(2000, 500).slideUp(500, function () {
            $(".alert-success").alert('close');
        });
        $(".alert-warning").fadeTo(2000, 500).slideUp(500, function () {
            $(".alert-warning").alert('close');
        });
    };
    //hiding the tickmarkend


//active tab function  
    $scope.currenttab_selection = function (tabname)
    {
        $scope.customers.current_tab = tabname;
        $scope.$apply(function () {
            $location.path('/' + tabname + '');
        });
    }
//end active tab    



    //active the subpanel tabs
    $scope.employpanel1selected = true;
    $scope.employheaders1 = true;
    //$scope.showincome1=true;
    //$scope.employincomepanel1selected = true;


    $scope.employpanel1 = function () {
        $scope.employheaders1 = true;
        $scope.showques1 = false;
        $scope.showincome1 = false;
        $scope.showaddincome1 = false;

        $scope.employpanel1selected = true;
        $scope.employincomepanel1selected = false;
        $scope.benefitspanel1selected = false;
        $scope.addincomepanel1selected = false;
    };
    $scope.employincomepanel1 = function () {
        $scope.showques1 = false;
        $scope.showincome1 = true;
        $scope.employheaders1 = false;
        $scope.showaddincome1 = false;

        $scope.employpanel1selected = false;
        $scope.employincomepanel1selected = true;
        $scope.benefitspanel1selected = false;
        $scope.addincomepanel1selected = false;
    };
    $scope.benefitspanel1 = function () {
        $scope.showques1 = true;
        $scope.showincome1 = false;
        $scope.employheaders1 = false;
        $scope.showaddincome1 = false;

        $scope.employpanel1selected = false;
        $scope.employincomepanel1selected = false;
        $scope.benefitspanel1selected = true;
        $scope.addincomepanel1selected = false;
    };

    $scope.addincomepanel1 = function () {
        $scope.showaddincome1 = true;
        $scope.showincome1 = false;
        $scope.employheaders1 = false;
        $scope.showques1 = false;

        $scope.employpanel1selected = false;
        $scope.employincomepanel1selected = false;
        $scope.benefitspanel1selected = false;
        $scope.addincomepanel1selected = true;
    };


//save to db

    $scope.wholedata = {};
    $scope.emp_header1 = function (employ, index)
    {
        console.log(employ);
        //$scope.header = true; // selection of tm	
        //$scope.emp_tab_initialization=({employment:'1'});
        //$scope.applicants[0].employment[0]=$scope.emp_tab_initialization;	
        //console.log($scope.applicants[0].employment[0]);
        $scope.wholedata = angular.copy(employ);
        //$scope.wholedata.userid = $scope.session; 

        $rootScope.eEmploymentStatusIDs1 = $scope.wholedata.employement_type;
        var dataobj = $scope.wholedata;
        $http.post('php_pages/employheader_db.php', dataobj).
                success(function (data, status) {
                    if ($rootScope.eEmploymentStatusIDs1 == '5' || $rootScope.eEmploymentStatusIDs1 == '6') {
                        $scope.addincomepanel1();
                        //console.log('employment',$rootScope.eEmploymentStatusIDs1);
                    } else {
                        $scope.employincomepanel1();
                        ///console.log('employment',$rootScope.eEmploymentStatusIDs1);
                    }

                    angular.element(document.getElementById('message')).append('<div class="alert alert-success"  id="success-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Employment saved</div>');
                    $scope.hidealert();

                });
    }

    $scope.wholedata1 = {};
    $scope.emp_annual = function (emp) {
        //$scope.annual=true;	tm
        $scope.wholedata1 = angular.copy(emp);
        $scope.session = 1;
        $scope.wholedata1.userid = $scope.session;
        var dataobj = $scope.wholedata1;
        $http.post('php_pages/employannual_db.php', dataobj).
                success(function (data, status) {
                    angular.element(document.getElementById('message')).append('<div class="alert alert-success"  id="success-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Employment Income saved</div>');
                    $scope.hidealert();
                    $scope.addincomepanel1();
                })
    }


    $scope.emp_additional = function (add) {
        console.log(add);
        //$scope.additional=true;	//tm
        $scope.wholedata = angular.copy(add);
        $scope.session = 1;
        $scope.wholedata.userid = $scope.session;
        var dataobj = $scope.wholedata;
        $http.post('php_pages/employadditional_db.php', dataobj).
                success(function (data, status) {
                    angular.element(document.getElementById('message')).append('<div class="alert alert-success"  id="success-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Additional Income saved</div>');
                    $scope.hidealert();
                    $scope.benefitspanel1();
                })
    }


    $scope.wholedata = {};
	
    $scope.emp_benefits = function (benfits, index) {
        console.log(benfits);
        $scope.session = 1;
        $scope.wholedata = angular.copy(benfits);
        $scope.wholedata.userid = $scope.session;
        $scope.wholedata.applicantid = 1;
        var dataobj = $scope.wholedata;
		
        console.log(dataobj);
        $http.post('php_pages/employbenefits_db.php', dataobj).
                success(function (data, status) {
                    angular.element(document.getElementById('message')).append('<div class="alert alert-success"  id="success-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Benefits saved</div>');
                    $scope.hidealert();
                    $scope.addbenefits(benefits, index);
                    $scope.showbenefitsone1 = 'false';

                    //$scope.customers.current_tab='/liabilities';
                })
    }

    $scope.wholedata = {};
    $scope.emp_selfemploy = function (self) {
        console.log('selfemployments', self);
        $scope.wholedata = angular.copy(self);
        $scope.wholedata.userid = $scope.session;
        var dataobj = $scope.wholedata;
        $http.post('php_pages/selfemployment_db.php', dataobj).
                success(function (data, status) {
                    angular.element(document.getElementById('message')).append('<div class="alert alert-success"  id="success-alert"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Success!</strong> Self Employed saved</div>');
                    $scope.hidealert = hidealert();
                    $scope.addincomepanel1(index);
                })
    }

    //add benefits multiple scenarios
	$scope.benefit_count=0;
    $scope.addbenefits = function (data, index)
    {
        console.log('benefitsaddeddata', data);
        console.log('benefitsaddedindex', index);
        //var count=parseInt($scope.benefit_count)+1;
		var count=Math.floor(100000 + Math.random() * 900000);
		console.log('count',count);
        //$scope.$apply(function () {
        $scope.customers.applicants[0].emploment[0].benifits.push({benefitid:count ,benefit: data.benifit, benefit_amt: data.benifit_amt});
        //benefit_id is unqiue id
        //change our model names
        //console.log('benefitsaddeddata',$scope.empdata);
        //$scope.benefits=$scope.empdata;
        /*$scope.benefitsdata.$save({
         benifit:data.benifit,
         beniifit_amt:data.beniifit_amt	
         });*/
        // console.log('benefits',$scope.benefitsdata);
        $scope.showbenefitsone1 = 'false';
        //$scope.benefit_count=count;
    };


    //delete benefits
    $scope.deleteadded_benefits = function (benefitsindex)
    {
        $scope.customers.applicants[0].emploment[0].benifits.splice(benefitsindex, 1);
    }

// Display the header type name in multiple scenario
    $scope.benefitstype = [{id: "26", name: 'Child Benefit'}, {id: "13", name: 'Child Tax Credit'}, {id: "25", name: 'Court Order Maintenance'}, {id: "14", name: 'Disability Living Allowance'}, {id: "15", name: 'Foster Parent Income'}, {id: "16", name: 'Housing Benefit'}, {id: "17", name: 'Incapacity Benefit'}, {id: "18", name: 'Pension Credit'}, {id: "19", name: 'Severe Disability Allowance'}, {id: "20", name: 'Statutory Maternity Pay'}, {id: "21", name: 'Statutory Paternity Pay'}, {id: "22", name: 'Statutory Sick Pay'}, {id: "27", name: 'Widow Benefit'}, {id: "23", name: 'Working Family Tax Credits'}];

    $scope.getBenefitType = function (benefitid)
    {
        $scope.result = "";
        angular.forEach($scope.benefitstype, function (benefits) {

            if (benefits.id == benefitid)
                $scope.result = benefits.name;
            console.log($scope.result);
        });
        return $scope.result;
    }


//add more button click		
    $scope.addMoreBenefits = function () {
              $scope.showbenefitsone1=true;
    };
    $scope.save_Benefits=function(emplomentbenifits,benifits){
        $scope.showbenefitsone1=false;
		var count=Math.floor(100000 + Math.random() * 900000);
		console.log('count',count);
        //$scope.$apply(function () {
        //emplomentbenifits.push({benefit_id:count ,benefit: benifitss.benifit, benefit_amt: benifitss.benifit_amt});
        emplomentbenifits.push(benifits);
        console.log(benifitss);
        //benifits.benefit_id=$scope.customers.applicants[applicantIndex].emploment.benifits.length+1;
       //$scope.customers.applicants[applicantIndex].emploment.benifits.push(benifits); 
    };
    $scope.deleteBenefits=function(applicantIndex,benefitIndex){
        $scope.customers.applicants[applicantIndex].emploment.benifits.splice(benefitIndex,1);
    };
    $scope.showbenefitsone1 = 'false';
    $scope.showbenefitsone = function (index) {

        var state = $(this).data('state');
        state = !state;
        if (state) {
            $scope.showbenefitsone1 = 'true';
        } else {
            $scope.showbenefitsone1 = 'false';
        }
        $(this).data('state', state);

    }

//keyup detecing
    var format = "dd/mm/yyyy";
    var match = new RegExp(format
            .replace(/(\w+)\W(\w+)\W(\w+)/, "^\\s*($1)\\W*($2)?\\W*($3)?([0-9]*).*")
            .replace(/m|d|y/g, "\\d"));
    var replace = "$1/$2/$3$4"
            .replace(/\//g, format.match(/\W/));

    function doFormat(target)
    {
        target.value = target.value
                .replace(/(^|\W)(?=\d\W)/g, "$10")   // padding
                .replace(match, replace)             // fields
                .replace(/(\W)+/g, "$1");            // remove repeats
    }

    $scope.datekeyup = function (event) {
//$("input[name='birthdate']:first").keyup(function(e) {   
        if (!event.ctrlKey && !event.metaKey && (event.keyCode == 32 || event.keyCode > 46))
            doFormat(event.target)
    };


});
